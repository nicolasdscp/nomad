# Get Started 

Install dependencies

```shell
composer i
```

Start server

```shell
php -S localhost:8080 -t ./src
```
or
```shell
make
```

