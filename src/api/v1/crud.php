<?php

require_once __DIR__ . '/../../../vendor/autoload.php';

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

$app = new \Slim\App();

$container = $app->getContainer();
$container['upload_directory'] = __DIR__ . '/uploads';

$app->get('/', function(Request $request, Response $response) {
    $directory = $this->get('upload_directory');

    $files = glob($directory.'/*');
    $ts = [];

    foreach ($files as $name) {
        array_push($ts, basename($name));
    }

    $response->getBody()->write(json_encode($ts));
    return $response;
});

$app->post('/', function(Request $request, Response $response) {
    $directory = $this->get('upload_directory');

    $uploadedFiles = $request->getUploadedFiles();

    // handle single input with single file upload
    $uploadedFile = $uploadedFiles['file'];
    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
        moveUploadedFile($directory, $uploadedFile);
    }

    return $response->withRedirect('http://localhost:8080', 301);
});

$app->delete('/{file}', function(Request $request, Response $response, array $args) {
    unlink($this->get('upload_directory').'/'.$args["file"]);
    return $response->withStatus(202);
});

/**
 * Moves the uploaded file to the upload directory and assigns it a unique name
 * to avoid overwriting an existing uploaded file.
 *
 * @param string $directory directory to which the file is moved
 * @param UploadedFile $uploadedFile file uploaded file to move
 * @return string filename of moved file
 */
function moveUploadedFile($directory, UploadedFile $uploadedFile)
{
    $filename = $uploadedFile->getClientFilename();

    if (pathinfo($filename, PATHINFO_EXTENSION) === "pdf") {
        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
    }
}

$app->run();

?>