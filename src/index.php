<!DOCTYPE html>
<html lang="en">

<head>
    <title>Uploader</title>
</head>

<body>
    <div style="width: 50%;display: inline-block;">
        <form action="http://localhost:8080/api/v1/crud.php" method="post" enctype="multipart/form-data">
            <!-- upload of a single file -->
            <p>
                <label>Add file: </label><br />
                <input type="file" name="file" />
            </p>
            <p>
                <input type="submit" />
            </p>
        </form>
    </div>
    <div style="width: 49%;display: inline-block;">
        <table>
            <thead>
                <tr>
                    <th colspan="2">File Name</th>
                    <th colspan="2">Action</th>
                </tr>
            </thead>
            <tbody id="tbd">
                <tr>
                    <td>The table body</td>
                    <td>with two columns</td>
                </tr>
            </tbody>
        </table>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous"></script>
    <script>

        function deleteFile(file) {
            axios.delete(`http://localhost:8080/api/v1/crud.php/${file}`)
                .then(() => document.getElementById(`row_${file}`).remove())
                .catch(e => console.log(e))  
        }

        function buildArray(files) {
            let tab = document.getElementById('tbd')
            let nextHtml = ''

            for (let i = 0; i < files.length; i++) {
                nextHtml = `${nextHtml}<tr id="row_${files[i]}"><td>${files[i]}<td><td><button onclick="deleteFile('${files[i]}')">Delete</button><td></tr>`
            }

            tab.innerHTML = nextHtml
        }

        axios.get('http://localhost:8080/api/v1/crud.php')
            .then(res => buildArray(res.data))
            .catch(e => alert(e))
    </script>
</body>

</html>